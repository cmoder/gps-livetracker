<?php
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$with_image = false;
$outputfile = readlink( "tracker_current.js" );
$timestamp = time();
$imagedest = substr( $outputfile, 0, strrpos( $outputfile, '.' ) ) . "/image$timestamp.jpg";
$imagelink = basename( $outputfile, ".js" ) . "/image$timestamp.jpg";
# FIXME: change the key
$key = 12345;

function range_check( $val, $min, $max )
{
	return ( isset( $val ) && is_numeric( $val ) && ($val >= $min) && ($val <= $max) );
}

(isset( $_GET['lang'] ) and preg_match( "/^de$|^en$/", $_GET['lang'] ) and $lang = $_GET['lang'] ) or $lang = 'de';
$input_lat = range_check( $_POST['lat'], -90, 90 ) ? $_POST['lat'] : (range_check( $_GET['lat'], -90, 90 ) && $_GET['key'] == $key ? $_GET['lat'] : NULL);
$input_lon = range_check( $_POST['lon'], -180, 360 ) ? $_POST['lon'] : (range_check( $_GET['lon'], -180, 360 ) && $_GET['key'] == $key ? $_GET['lon'] : NULL);
$input_acc2 = range_check( $_POST['acc2'], 0, 100000 ) ? $_POST['acc2'] : (range_check( $_GET['acc2'], -180, 360 ) && $_GET['key'] == $key ? $_GET['acc2'] : 10000);
$input_comment = isset( $_POST['comment'] ) ? $_POST['comment'] : (isset( $_GET['comment'] ) && $_GET['key'] == $key ? $_GET['comment'] : '');
$input_new = range_check( $_POST['new'], 0, 2 ) ? $_POST['new'] : (range_check( $_GET['new'], 0, 2 ) ? $_GET['new'] : 0);


# parse autotracking code
# FIXME: if changing the comment parameter, you have to edit also these lines
if( $_GET['auto'] == 1 and preg_match( '/time(\d+)hdop([\d.]+)altitude([\d.]+)speed([\d.]+)/', $input_comment, $m ) )
	$input_comment = sprintf( array( 'de' => "Zeit: %s GMT\nHDOP: %f\nH&ouml;he: %.1f&nbsp;m\nGeschwindigkeit: %.1f&nbsp;km/h", 'en' => "Time: %s GMT\nHDOP: %f\nAltitude: %.1f&nbsp;m\nSpeed: %.1f&nbsp;km/h" )[$lang], gmstrftime( "%F %T", $m[1] / 1000 ), $m[2], $m[3], $m[4] * 3.6 );

if( isset( $input_lat ) && isset( $input_lon ) )
{
	# FIXME: edit the domain names!
	setcookie( 'lat', $input_lat, time() + 60 * 60 * 24, '', '.website.com' );
	setcookie( 'lon', $input_lon, time() + 60 * 60 * 24, '', '.website.com' );

	if( $_FILES['img']['error'] == UPLOAD_ERR_OK )
	{
		$imagesize = @getimagesize( $_FILES['img']['tmp_name'] );

		if( ($imagesize['mime'] == "image/jpeg") && @move_uploaded_file( $_FILES['img']['tmp_name'], $imagedest ) )
		{
			$with_image = true;

			// rotate image according to EXIF orientation marker, and get image size again
			@exec( "exiftran -aip \"$imagedest\"" );
			$imagesize = @getimagesize( $imagedest );

			$image_scale = ( $imagesize[0] > $imagesize[1] ? 320 : round( 320 * $imagesize[0] / $imagesize[1] ) );
		}
	}

	$querystring = "http://api.openweathermap.org/data/2.5/weather?lat=$input_lat&lon=$input_lon&mode=xml&units=metric&lang=$lang&cnt=1";
	$apiquery = file_get_contents( $querystring );
	$openweathermap_api = simplexml_load_string( $apiquery );

	$data  = "points.push( [ $input_lat, $input_lon ] );";
	$data .= "accuracy.push( " . (0 + $input_acc2) . " );";
	$data .= "texts.push( '" . strtr( $input_comment, array( "\r\n" => "<br />", "\r" => "<br />", "\n" => "<br />", "'" => "\'" ) ) . "' );";
	$data .= "times.push( '" . date( "Y-m-d H:i", $timestamp ) . "' );";
	$data .= "mapimages.push( '" . ($with_image ? $imagelink : "") . "' );";
	$data .= "mapimagewidths.push( " . ($with_image ? $image_scale : -1) . " );";
	$data .= $input_new > 0 ? "newseg[points.length-1] = $input_new;" : '';

	# Openweathermap API to JavaScript
	$weather['de'] = ($openweathermap_api->weather ? "weather.push( [ '(in " . $openweathermap_api->city['name'] . ") " . $openweathermap_api->weather['value'] . "', " . round( $openweathermap_api->temperature['value'], 1 ) . ", 'Luftfeuchtigkeit: " . $openweathermap_api->humidity['value'] . "%', 'Wind: " . $openweathermap_api->wind->direction['code'] . " mit " . ($openweathermap_api->wind->speed['value'] * 3.6) . "&nbsp;km/h', '" . $openweathermap_api->lastupdate['value'] . "' ] );" : "weather.push( '' );" );
	$weather['en'] = ($openweathermap_api->weather ? "weather.push( [ '(in " . $openweathermap_api->city['name'] . ") " . $openweathermap_api->weather['value'] . "', " . round( $openweathermap_api->temperature['value'], 1 ) . ", 'Humidity: " . $openweathermap_api->humidity['value'] . "%', 'Wind: " . $openweathermap_api->wind->direction['code'] . " with " . ($openweathermap_api->wind->speed['value'] * 3.6) . "&nbsp;km/h', '" . $openweathermap_api->lastupdate['value'] . "' ] );" : "weather.push( '' );" );
	$data .= $weather[$lang];

	# automatic position update: mark this entry as automatic
	if( isset($_GET['auto']) )
	{
		$data .= "autopos[points.length-1] = 1;";
	}

	# comment entry out if no data has been passed, e.g. due to a poor network connection that has aborted before the image has been uploaded
	if( empty($input_comment) and $with_image === false and $input_new === 0 )
	{
		$data = "//$data";
	}

	# atomic write operation, locks the file
	file_put_contents( $outputfile, "$data\n", FILE_APPEND | LOCK_EX );
}

$field_lat = range_check( $input_lat,  -90,  90 ) ? $input_lat : range_check( $_COOKIE['lat'],  -90,  90 ) ? $_COOKIE['lat'] : '';
$field_lon = range_check( $input_lon, -180, 360 ) ? $input_lon : range_check( $_COOKIE['lon'], -180, 360 ) ? $_COOKIE['lon'] : '';

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, user-scalable=yes" />
	<meta name="HandheldFriendly" content="true" />
	
	<style type="text/css">
	/*<![CDATA[*/
		* { margin: 0px; padding: 0px; border-style: none; border-width: 0px; }
		form > * { border-style: solid; border-width: 1px; }
	/*]]>*/
	</style>

	<script type="text/javascript">
	// <![CDATA[

	function leadingZero(val)
	{
		return (val < 10 ? "0" + val : val );
	}

	function setPosition(position)
	{
		document.getElementById("lat").value = position.coords.latitude;
		document.getElementById("lon").value = position.coords.longitude;
		t = new Date( position.timestamp );
		document.getElementById("acc").value = position.coords.accuracy + "m, " + t.toISOString();
		document.getElementById("acc2").value = position.coords.accuracy;
	}

	function getPosition( highaccuracy )
	{
		if( navigator.geolocation )
		{
			navigator.geolocation.getCurrentPosition( 
				// success
				setPosition,
			function(position_error) {
				// error
				if( highaccuracy == true )
				{
					getPosition( false );
				}
				else
				{
					document.getElementById("acc").value = "--- (err. " + position_error.code + ")";
				}
			},{
				enableHighAccuracy: highaccuracy
			});
		}
	}

	function resize()
	{
		var screenNetWidth = parseInt( screen.availWidth ) - 14;

		document.getElementById('lat').style.width = screenNetWidth / 2 - 2 + "px";
		document.getElementById('lat').style.height = screen.availHeight / 8 + "px";
		document.getElementById('lon').style.width = screenNetWidth / 2 - 2 + "px";
		document.getElementById('lon').style.height = screen.availHeight / 8 + "px";
		document.getElementById('acc').style.width = "80%";
		document.getElementById('img').style.width = screenNetWidth - 2 + "px";
		document.getElementById('img').style.height = screen.availHeight / 8 + "px";
		document.getElementById('comment').style.width = screenNetWidth - 2 + "px";
		document.getElementById('pos').style.width = screenNetWidth - 2 + "px";
		document.getElementById('pos').style.height = screen.availHeight / 8 + "px";
		document.getElementById('subm').style.width = screenNetWidth - 2 + "px";
		document.getElementById('subm').style.height = screen.availHeight / 8 + "px";
	}

	function getCookie()
	{
		if( document.cookie )
		{
			var ca = document.cookie.split(';');
			
			for(var i=0;i < ca.length;i++) {
				var c = ca[i];
				while (c.charAt(0)==' ')
					c = c.substring(1,c.length);

				if( c.indexOf('lat=') == 0 )
					document.getElementById("lat").value = c.substring( 4, c.length );
				
				if( c.indexOf('lon=') == 0 )
					document.getElementById("lon").value = c.substring( 4, c.length );
			}
		}
	}

	// ]]>
	</script>

	<title>GPS Tracker Input</title>
</head>

<body onload="resize(); getPosition( true );" onresize="resize();" id="body">
<form action="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME']; ?>" method="post" enctype="multipart/form-data" accept-charset="utf8">
		<input id="pos" type="button" value="Position bestimmen" onClick="getPosition( true );" />
		<input name="lat" id="lat" type="text" size="15" maxlength="30" value="<?php echo $field_lat; ?>" placeholder="lat" /><input name="lon" id="lon" type="text" size="15" maxlength="30" value="<?php echo $field_lon; ?>" placeholder="lon" />
		<input name="acc" id="acc" type="text" size="30" maxlength="30" value="accuracy" readonly onClick="getPosition();" /><input name="new" id="new" type="checkbox" /><label for="new">New Track</label>
		<textarea name="comment" id="comment" rows="4" cols="35"></textarea>
		<input name="img" id="img" type="file" accept="image/*" />
		<input name="acc2" id="acc2" type="hidden" />
		<input id="subm" type="submit" />
	</form>
</body>
</html>
