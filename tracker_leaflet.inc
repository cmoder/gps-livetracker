<?php

$protocol = isset($_SERVER['HTTPS']) ? "https" : "http";

$rain_kml = "$protocol://google.meteox.com/meteox.kmz?time=" . @mktime();

echo <<<EOS1
	<link rel="stylesheet" type="text/css" href="$root/tracker/leaflet/leaflet.css" />
	<style type="text/css">
	/*<![CDATA[*/
		.leaflet-control-zoom a[href] { color: black; text-decoration: inherit; }	/* zoom buttons: black, instead of links */
		.leaflet-control-layers	{ text-align: left; }			/* layer control: show the layer names left aligned */
		.leaflet-popup-content { font-size: initial; }			/* larger font in popups */
		.leaflet-div-icon	{ border: none; background: none; }	/* remove DIV of icons */

		#gibs_controls	{ background-color: #fff; border-radius: 5px; box-shadow: 0 1px 7px rgba(0,0,0,0.65); padding: 6px; display: none; }	/* date controls for GIBS satellite overlay */
		.mIcon	{ stroke-width: 1px; stroke: #000; fill-opacity: 0.7; }	/* marker for cross-country tracks */
		.circlemarker { stroke-width: 0; border-radius: 50%; width: 10px; height: 10px; }	/* tracker, shape of the autotrack circles */
	/*]]>*/
	</style>

	<!-- JSZip and JSZipUtils for decoding KMZ files; Leaflet for displaying the map; Leaflet-Omnivore for displaying KML and polylines -->
	<script src="$root/tracker/leaflet/jszip.min.js" type="text/javascript"></script>
	<script src="$root/tracker/leaflet/jszip-utils.min.js" type="text/javascript"></script>
	<script src="$root/tracker/leaflet/leaflet.js" type="text/javascript"></script>
	<script src="$root/tracker/leaflet/leaflet-omnivore.min.js" type="text/javascript"></script>
	<script src="$root/tracker/leaflet/GIBSMetadata.js" type="text/javascript"></script>
	<script src="$root/tracker/leaflet/GIBSLayer.js" type="text/javascript"></script>

	<SCRIPT type="text/javascript">
	// <![CDATA[

	var map;
	var layer_osm;
	var layer_otm;
	var layer_shading;
	var rain;
	var show_rain = false;
	var show_shading = false;
	var show_contours = false;
	var zoom_out = false;

	var points = new Array();
	var accuracy = new Array();
	var texts = new Array();
	var times = new Array();
	var mapimages = new Array();
	var mapimageorient = new Array();
	var weather = new Array();
	var marker = new Array();
	var acccircle = new Array();
	var infowindow;
	var infowindow_index;
	var infowindow_updated = false;
	var iwcontent = new Array();
	var autopos = new Array();
	var content_markers = new Array();
	var content_marker_indices = new Array();
	var newseg = new Array();
	var seglength = new Array();
	var polyline;
	var kml;
	var spinner;

	function resize_map_to_full_window()
	{
		document.getElementById("map").style.position = "absolute";
		document.getElementById("map").style.left = "0px";
		//document.getElementById("map").style.width = document.getElementsByTagName("body")[0].style.width + "px";
		document.getElementById("map").style.width = window.innerWidth + "px";
		document.getElementById("map").style.height = window.innerHeight + "px";
		document.getElementById("map").style.marginBottom = "0px";
		//document.getElementById("mapcontainer").style.height = window.innerHeight + "px";
	}

	function deg2rad(deg)
	{
		return deg / 180 * Math.PI;
	}

	function calc_dist(lat1, lon1, lat2, lon2)
	{
		return 6371 * Math.acos( Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(lon2 - lon1)) );
	}

	function get_next_marker( index )
	{
		return content_markers[(content_marker_indices[index] + 1) % content_markers.length];
	}

	function get_prev_marker( index )
	{
		// because of symmetric modulo, use operator twice for negative values
		return content_markers[((content_marker_indices[index] - 1) % content_markers.length + content_markers.length) % content_markers.length];
	}

	function get_next_image_marker( index )
	{
		var i = index;
		do {
			i = get_next_marker( i );
		} while (mapimages[i] === '' || i == index);
		return i;
	}

	function get_prev_image_marker( index )
	{
		var i = index;
		do {
			i = get_prev_marker( i );
		} while (mapimages[i] === '' || i == index);
		return i;
	}

	function get_next_content_marker( index )
	{
		var i = index;
		do {
			i = get_next_marker( i );
		} while ((mapimages[i] === '' && texts[i] === '') || i == index);
		return i;
	}

	function get_prev_content_marker( index )
	{
		var i = index;
		do {
			i = get_prev_marker( i );
		} while ((mapimages[i] === '' && texts[i] === '') || i == index);
		return i;
	}

	function update_lightbox( index )
	{
		spinner.display = 'block';
		document.getElementById('lightboximg').src = mapimages[index];
		document.getElementById('lightboximg').title = marker[index].options.title;
		document.getElementById('lightboximg').onload = function() { spinner.display = 'none'; };
		document.getElementById('lightboximg').onerror = function() { spinner.display = 'none'; };
		document.getElementById('lb_prev').onclick = function() { update_lightbox( get_prev_image_marker(index) ); };
		document.getElementById('lb_next').onclick = function() { update_lightbox( get_next_image_marker(index) ); };
		infowindow_position_content(index, false);
	}

	function infowindow_position_content( index, lightbox = true )
	{
		// move map to marker
		map.panTo(marker[index].getLatLng());

		// create popup
		infowindow_index = index;
		infowindow_updated = false;

		infowindow = L.popup( {
			// set popup size according to image size; if no image, use the image with default value of 40% viewport height
			minWidth: (mapimageorient[index] == -1 ? Math.round(0.4 * window.innerHeight) : 'auto'),
			// move the popup 30px up, so it is not on the tip of the marker, but further up
			offset: [0, -30],
			// when panning to the popup, keep 15% of the map size below, so the marker is not
			// at the very bottom; since the popup window is wider and goes further up, no padding
			// in these directions is required
			autoPanPaddingBottomRight: [0, Math.round(0.15 * map.getSize().y)]
		} );

		// move the popup to the marker position
		infowindow.setLatLng(marker[index].getLatLng());

		// fill the popup with content
		infowindow.setContent( iwcontent[index] );

		// show the popup
		infowindow.openOn( map );

		// put current image in lightbox
		if (lightbox)
			update_lightbox( index );
	}

	function update_gibs()
	{
		layer_gibs.forEach( l => l.setDate(new Date(
			document.getElementById('gibs_year').value,
			document.getElementById('gibs_month').value - 1,
			document.getElementById('gibs_day').value))
		);
	}

	function map_create_base()
	{
		// set body width to maximum and disable/enable scrolling, to get scrollbar width
		maxWidth = getComputedStyle( document.body ).maxWidth;
		document.body.style.maxWidth = "none";
		document.body.style.overflow = "hidden";
		innerWidth1 = parseInt( getComputedStyle( document.body ).width );
		document.body.style.overflow = "initial";
		innerWidth2 = parseInt( getComputedStyle( document.body ).width );
		document.body.style.maxWidth = maxWidth;
		scrollbarWidth = innerWidth1 - innerWidth2;

		// reset maxWidth and Width from the values inherited from body, before creating the map
		document.getElementById("map").style.maxWidth = (window.innerWidth - scrollbarWidth) + "px";
		document.getElementById("map").style.width = (window.innerWidth - scrollbarWidth) + "px";

		map = L.map('map');

		layer_osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			'useCache': true
		});
		layer_otm = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
			'useCache': true
		});
		layer_esri_satellite = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}.jpg', {
			'useCache': true
		});

		// https://wiki.earthdata.nasa.gov/display/GIBS/GIBS+Available+Imagery+Products:
		// Type: WMTS; Projection: epsg3857; TileMatrixSet: see http://gexplor.fr/servreg/servreg.php/nasa-gibs-best-wmts
		// Zoom zu gering => z.B. auf Level 9 => "maxNativeZoom" angeben, und in URL hardcoden "GoogleMapsCompatible_Level9"
		layer_gibs = [
			new L.GIBSLayer('MODIS_Aqua_CorrectedReflectance_TrueColor', { maxNativeZoom: 9 }),
			new L.GIBSLayer('MODIS_Terra_NDSI_Snow_Cover', { maxNativeZoom: 8 }),
		];

		layer_shading = L.layerGroup(
			[
				L.tileLayer('https://tiles.wmflabs.org/hillshading/{z}/{x}/{y}.png', {
					'useCache': true
				}),
				L.tileLayer('https://tiles.wmflabs.org/hillshading/{z}/{x}/{y}.png', {
					'useCache': true
				})
			]);

		layer_gibs.forEach( l => {
			l.on('add', function() { document.getElementById('gibs_controls').style.display = 'block'; } );
			l.on('remove', function() {
					keepGibsControls = false;
					layer_gibs.forEach( l => keepGibsControls = keepGibsControls || map.hasLayer(l) );
					if (!keepGibsControls)
						document.getElementById('gibs_controls').style.display = 'none';
				} );
			});

		gibs_control = new L.Control({ position: 'topright'});

		gibs_control.onAdd = function(map) {
				var c = L.DomUtil.create('div');
				var d = new Date();
				d.setDate(d.getDate() - 1);	// set date to yesterday, since satellite images of today are probably missing
				c.id = 'gibs_controls';

				var html = '<input id="gibs_year" type="number" style="width: 4em;" min="2000" max="';
				html += d.getFullYear();
				html += '" value="' + d.getFullYear();
				html += '"><input id="gibs_month" type="number" style="width: 2em;" min="1" max="12"';
				html += ' value="' + (d.getMonth() + 1) + '">';
				html += '<input id="gibs_day" type="number" style="width: 2em;" min="1" max="31"';
				html += ' value="' + d.getDate() + '">';
				c.innerHTML = html;

				return c;
			};

		gibs_control.addTo(map);
		update_gibs();

		// default layer: OSM
		map.addLayer( layer_osm );

		L.control.layers( {
			'Openstreetmap': layer_osm,
			'Opentopomap': layer_otm,
			'Satellit': layer_esri_satellite,
			'MODIS TrueColor': layer_gibs[0],
		}, {
			'Schattierung': layer_shading,
			'MODIS Snow Cover': layer_gibs[1],
		} ).addTo(map);

		window.onresize = resize_map_to_full_window;
		resize_map_to_full_window();

		L.DomEvent.on(document.getElementById('gibs_year'), 'change', function(e) { update_gibs(); } );
		L.DomEvent.on(document.getElementById('gibs_month'), 'change', function(e) { update_gibs(); } );
		L.DomEvent.on(document.getElementById('gibs_day'), 'change', function(e) { update_gibs(); } );

		L.DomEvent.on(document.getElementById('gibs_controls'), 'wheel', L.DomEvent.stopPropagation );
		L.DomEvent.on(document.getElementById('gibs_controls'), 'click', L.DomEvent.stopPropagation );
		L.DomEvent.on(document.getElementById('gibs_controls'), 'dblclick', L.DomEvent.stopPropagation );

		var el = document.createElement('div');
		el.setAttribute('id', 'spinner');
		el.appendChild(document.createElement('div'));
		document.body.appendChild(el);
		spinner = el.style;
	}

	function map_add_tracker()
	{
		var color_index = 0;
		var colors = [ "#FF004F", "#FFCF00" ];

		if( points && 0 != points.length )
		{
			map.setView( [ points[points.length - 1][0], points[points.length - 1][1] ], 10 );

			// If the line has more than 2 points:
			if( points.length >= 2 )
			{
				for( var i in points )
				{
					// If it is the first point (overall, or of the segment):
					if( i == 0 || newseg[i] )
					{
						// If it is not the first point, add a line from the last point in the old color, and finalize the line (= add to the map).
						if( i > 0 )
						{
							// Add the connecting line segment only if newseg is 1, i.e. new color, but not interrupted.
							if( newseg[i] == 1 )
								polyline.addLatLng( [ points[i][0], points[i][1] ] );

							polyline.addTo( map );
						}

						// Then start a new line segment.
						polyline = L.polyline( [], { color: colors[color_index], weight: 3, opacity: 1.0 } );

						color_index++;
						color_index %= colors.length;
					}
					// If it is a point in the middle, calculate the distance since the last point, and store it.
					else
					{
						seglength[i] = calc_dist( points[i-1][0], points[i-1][1], points[i][0], points[i][1] );
					}

					// Add the point to the polyline; either to the current one, or, if we are at the beginning, the newly created one.
					polyline.addLatLng( [ points[i][0], points[i][1] ] );
				}

				// At the end: finalize the current polyline by adding it to the map.
				polyline.addTo( map );

				map.setView( [ (points[points.length - 1][0] + points[points.length - 2][0]) / 2, (points[points.length - 1][1] + points[points.length - 2][1]) / 2 ], 15 );
			}

			color_index = 0;

			for( var i in points )
			{
				if( newseg[i] )
				{
					color_index++;
					color_index %= colors.length;
				}

				var markertext = texts[i].replace(/<br \/>/g, "\\n")
							.replace(/<[^<>]+>/g, "")
							.replace(/&nbsp;/g, " ")
							.replace(/&auml;/g, "ä")
							.replace(/&ouml;/g, "ö")
							.replace(/&uuml;/g, "ü")
							.replace(/&Auml;/g, "Ä")
							.replace(/&Ouml;/g, "Ö")
							.replace(/&Uuml;/g, "Ü")
							.replace(/&szlig;/g, "ß")
							.replace(/&eacute;/g, "é")
							.replace(/&egrave;/g, "è")
							.replace(/&aacute;/g, "á")
							.replace(/&agrave;/g, "à")
							.replace(/&ccedil;/g, "ç")
							.replace(/&ndash;/g, "–")
							.replace(/&mdash;/g, "—")
							.replace(/&rsquo;/g, "’")
							.replace(/&bdquo;/g, "„")
							.replace(/&ldquo;/g, "“")
							.replace(/&rdquo;/g, "”")
							.replace(/&euro;/g, "€");

				// create markers: real markers for manual points, circles for autopos
				if( !autopos[i] )
					marker[i] = L.marker( [ points[i][0], points[i][1] ], { title: markertext } );
				else {
					marker[i] = L.marker( [ points[i][0], points[i][1] ], { icon: L.divIcon({ className: 'circlemarker', iconSize: [10, 10], html: '<div class="circlemarker" style="background-color: ' + colors[color_index] + '" title="' + markertext + '"></div>'}) });
				}

				marker[i].addTo(map);

				// create infowindow only for manual points
				if( !autopos[i] )
				{
					// record which of the markers are content markers
					content_markers.push( i );
					content_marker_indices[i] = content_markers.length - 1;

					// FIXME: add accuracy
					// accuracy: only if the points have not been sent automatically
					if( accuracy[i] )
					{
/*						acccircle[i] = new google.maps.Circle( {
								center: new google.maps.LatLng( points[i][0], points[i][1] ),
								radius: accuracy[i],
								fillColor: colors[color_index],
								fillOpacity: 0.25,
								strokeColor: colors[color_index],
								strokeOpacity: 0,
								strokeWeight: 0,
								map: map
						} );*/
					}

					// overflow: so the div container surrounds the image
					iwcontent[i]  = '<div id="content" style="color:#000000; overflow:auto;"><div id="siteNotice" style="font-weight:bold;">';

					// links to previous and next marker
					iwcontent[i] += '<span style=""><a href="' + window.location.hash + '" onClick="infowindow_position_content( get_prev_content_marker(' + i + ') ); return false;">&lt;&lt;</a></span>';
					iwcontent[i] += '&nbsp;' + times[i] + '&nbsp;';
					iwcontent[i] += '<span style=""><a href="' + window.location.hash + '" onClick="infowindow_position_content( get_next_content_marker(' + i + ') ); return false;">&gt;&gt;</a></span>';
					iwcontent[i] += '</div>';

					// the content can be clicked to close the window; does not work with the whole window, since the prev/next links must also receive click events
					iwcontent[i] += '<div id="bodyContent" style="text-align:left; margin-top:1ex; overflow:auto;">';

					// if the marker contains an image, add it
					if(mapimageorient[i] > -1) {
						spinner.display = 'block';	// switch spinner on
						iwstring = mapimageorient[i] ? 'width:40vh;' : 'height:40vh;';	// make image size dependent on screen height, and same for portrait and landscape

						// add image; if loaded, update the infowindow position; "onloadstart" would be better, but this is invoked by update() again
						iwcontent[i] += '<a href="#showimage"><img id="popupimg' + i + '" src="' + mapimages[i] + '" style="float:right; margin:10px; ' + iwstring + '" onLoad="if (infowindow_updated == false) { infowindow_updated = true; infowindow.update(); } spinner.display = \'none\';"  onError="if (infowindow_updated == false) { infowindow_updated = true; infowindow.update(); } spinner.display = \'none\';" /></a>';
					}

					iwcontent[i] += texts[i];
					iwcontent[i] += ( weather[i].length ? '<p><i><b>Wetter:</b> ' + weather[i][0] + ', ' + weather[i][1] + '&nbsp;&deg;C, ' + weather[i][3] + '</i></p>' : '' );
					iwcontent[i] += '</div></div>';

					// if the marker is clicked, fill the infowindow and the light box with the appropriate content, and open the window
					marker[i].on( 'click', (function (index) { return function() { infowindow_position_content(index); } } )(i) );
				}
			}
		}

		// add lightbox; as last node, so it overlays the map and all the rest
		var lightboxnode = document.createElement('div');
		lightboxnode.id = 'showimage';
		lightboxnode.className = 'lightbox';
		lightboxnode.appendChild( document.createElement('a') );
		lightboxnode.firstChild.onclick = function() { window.history.back(); };
		lightboxnode.firstChild.appendChild( document.createElement('img') );
		lightboxnode.firstChild.firstChild.id = 'lightboximg';

		var lightbox_prevnext = document.createElement('div');
		lightbox_prevnext.className = 'lb_prevnext';

		lightbox_prevnext.appendChild( document.createElement('a') );
		lightbox_prevnext.lastChild.appendChild( document.createElement('div') );
		lightbox_prevnext.lastChild.lastChild.className = 'lb_button';
		lightbox_prevnext.lastChild.lastChild.id = 'lb_prev';
		lightbox_prevnext.lastChild.lastChild.title = 'Previous';

		lightbox_prevnext.appendChild( document.createElement('a') );
		lightbox_prevnext.lastChild.appendChild( document.createElement('div') );
		lightbox_prevnext.lastChild.lastChild.className = 'lb_button';
		lightbox_prevnext.lastChild.lastChild.id = 'lb_next';
		lightbox_prevnext.lastChild.lastChild.title = 'Next';

		lightboxnode.appendChild( lightbox_prevnext );

		document.getElementsByTagName("body")[0].appendChild( lightboxnode );

		document.onkeydown = function (e) {
			if (e.key == 'Escape') {
				if (document.location.hash === '#showimage')
					window.history.back();
				else
					infowindow.close();
			}
			else if (e.key == 'ArrowLeft') {
				if (infowindow.isOpen()) {
					infowindow_position_content( document.location.hash === '#showimage' ? get_prev_image_marker(infowindow_index) : get_prev_content_marker(infowindow_index) );
				}
			}
			else if (e.key == 'ArrowRight') {
				if (infowindow.isOpen()) {
					infowindow_position_content( document.location.hash === '#showimage' ? get_next_image_marker(infowindow_index) : get_next_content_marker(infowindow_index) );
				}
			}
		};
	}

	function map_add_kml(file, resize)
	{
		if (file.split('.').pop() === 'kmz')
		{
			JSZipUtils.getBinaryContent(file, function(err, data) {
				try {
					JSZip.loadAsync(data).then(function(zip) {
						return zip.file(/.*\.kml/)[0].async("string");
					}).then(function success(text) {
						kml = omnivore.kml.parse(text);
						if (resize)
							kml.on('ready', function() { resize2kml(); });
						kml.addTo(map);
					}, function error(e) {});
				} catch(e) {}
			});
		}
		else
		{
			kml = omnivore.kml(file);

			if (resize)
				kml.on('ready', function() { resize2kml(); });

			kml.addTo(map);
		}

	}

	function create_map(zoom_kml)
	{
		map_create_base();

		if( "$use_kml" )
			map_add_kml('$kml_file', zoom_kml);

		// set position only now, so onload events are fired
		map.setView([ $midpoint ], 10);

		// draw tracker and markers
		map_add_tracker();

		// update markers when zoomed
		map.on('zoomend', function() {
			map_update_markers();
		} );

		spinner.display = 'none';

	}

	// FIXME: does not work completely well
	function map_update_markers()
	{
		// one tile = 256 px; => zoom 0 = whole Earth
		var remove_threshold = 20 * (40000 / 256) / 2**map.getZoom();
		var sumlength = 0;

		for( var i in points )
		{
			// Punkte weglassen, wenn sie zu dicht aneinander liegen
			// 1. wenn vorher ein Marker ist, und zu dicht
			// 2. wenn danach ein Marker ist, und zu dicht
			// 3. wenn die aufsummierte Entfernung zu kurz

			if( map.hasLayer(marker[i]) &&
				autopos[i] &&
				i > 0 &&
				i < points.length &&
				((!autopos[i-1] && seglength[i] < remove_threshold) ||
				(!autopos[i+1] && seglength[i+1] < remove_threshold) ||
				(sumlength + seglength[i] < remove_threshold))
			)
			{
				map.removeLayer(marker[i]);

				// Entfernung seit letztem Punkt
				sumlength += seglength[i];
			}
			else
			{
				if (!map.hasLayer(marker[i]))
					map.addLayer(marker[i]);

				sumlength = 0;
			}
		}
	}

	function toggle_rain_kml()
	{
		if( show_rain == false )
		{
			if( ! rain )
			{
				rain = new google.maps.KmlLayer('$rain_kml');
				rain.set( 'preserveViewport', true );
			}

			rain.setMap( map );
			show_rain = true;
		}
		else
		{
			rain.setMap( null );
			show_rain = false;
		}
	}

	function toggle_rain_wms()
	{
		show_rain = show_rain ? false : true;
		renew_overlays();
	}

	function toggle_contours()
	{
		show_contours = show_contours ? false : true;
		renew_overlays();
	}

	function shading_on()
	{
		if( !map.hasLayer( layer_shading ) )
			map.addLayer( layer_shading );
	}

	function toggle_shading()
	{
		//show_shading = show_shading ? false : true;
		//renew_overlays();

		if( !map.hasLayer( layer_shading ) )
			map.addLayer( layer_shading );
		else
			map.removeLayer( layer_shading );

	}

	function renew_overlays()
	{
		if( !map.hasLayer(layer_shading) && show_shading == true )
		{
			map.addLayer( layer_shading );
		}
		else if( map.hasLayer(layer_shading) && show_shading == false )
		{
			map.removeLayer( layer_shading );
		}

		if( show_contours == true )
		{
			//map.overlayMapTypes.push( contoursMapType );
		}

		if( show_rain == true )
		{
			//map.overlayMapTypes.push( owmprecipitationMapType );
			//map.overlayMapTypes.push( owmpressureMapType );
		}
	}

	function resize2track()
	{
		viewport = L.latLngBounds( L.latLng( Math.min.apply(Math, points.map(function(v) { return v[0]; })), Math.min.apply(Math, points.map(function(v) { return v[1]; })) ), L.latLng( Math.max.apply(Math, points.map(function(v) { return v[0]; })), Math.max.apply(Math, points.map(function(v) { return v[1]; })) ) );

		map.fitBounds(viewport);
	}

	// attention: works only after KML layer is loaded
	function resize2kml()
	{
		map.fitBounds( kml.getBounds() );
	}

	function toggle_resize2kml()
	{
		if( zoom_out == false )
		{
			current_center = map.getCenter();
			current_zoom   = map.getZoom();

			if( kml )
				resize2kml();
			else
				resize2track();

			zoom_out = true;
		}
		else
		{
			map.setView( current_center, current_zoom );
			zoom_out = false;
		}
	}

	// ]]>
	</SCRIPT>

EOS1;

# FIXME: has to be deactivated for other maps
echo "\t<SCRIPT type=\"text/javascript\">\n";
readfile( basename( $_SERVER['SCRIPT_NAME'], ".php" ) . ".js" );
echo "\t</SCRIPT>\n\n";

$layerswitcher_string = '<A href="#" target="_self" onClick="toggle_shading();return false;">Schattierung ein/ausschalten</A>' . ($use_kml === 'true' ? ' | <A href="#" onClick="toggle_resize2kml();return false;">Zoom ganze Tour/Detail</A>' : '');
$layerswitcher_string_en = '<A href="#" target="_self" onClick="toggle_shading();return false;">shading on/off</A>' . ($use_kml === 'true' ? ' | <A href="#" onClick="toggle_resize2kml();return false;">zoom whole tour/detail</A>' : '');
?>
