GPS Livetracker
===============
What Is It?
-----------
A simple website that displays GPS positions, which can be uploaded manually or in an automated way.


How Does It Work?
-----------------
The livetracker consists of three components:
* Map: A website that displays the GPS coordinates on a map provided by Openstreetmap.
* Overview: A website that lists all maps in the _tracker/_ subdirectory.
* Submission form: A website for submitting a position marker, possibly together with some text and/or a picture. This form is optimized for using on a mobile device, and can also be used for automatic submission with tools like _OsmAnd_.

It works like this:
* The individual tracker map is a PHP webpage, which can be customized.
* The map includes a PHP file named *tracker_leaflet.inc* (or *tracker_googlemaps.inc*), which contains mostly Javascript. That means, the core functionality is implemented there.
* The map includes also a Javascript file with the coordinates. This file has the same name as the map, only a different extension (_js_ instead of _php_).
* The form for submitting coordinates (either manually or automatically) simply appends a line to the Javascript file with the coordinates.
* That means, every line of the Javascript file is independent; the arrays with the coordinates etc. are created while running the Javascript code.
* Uploaded images are copied into a subdirectory with the same name as the map.


Basic Configuration
--------------------
* Copy the files on your PHP-enabled webserver.
* In the file *tracker_form.php*: change the variable _$key_ to an appropriate value.
* In the file *tracker_form.php*: change the domain name in the *setcookie()* function call to your domain.
* Make sure the subdirectory *tracker/* is readable for the PHP script, so it can search for tracker files there.
* For *Leaflet* maps, download and install the following include files in the *leaflet/* subdirectory:
   * [Leaflet](https://leafletjs.com/) itself (tested with version 1.6)
   * [Leaflet-Omnivore](https://github.com/mapbox/leaflet-omnivore) for reading KML files and displaying polylines (tested with version 0.3.4)
   * [JSZip](https://stuk.github.io/jszip/) and [JSZipUtils](http://stuk.github.io/jszip-utils/) for reading compressed KMZ files (tested with versions 3.2.2/0.1.0)
   * [Leaflet-GIBS](https://github.com/aparshin/leaflet-GIBS) for including *NASA EOSDIS GIBS* satellite images.
* Edit the files as desired, add a stylesheet, ...


Create a Tracker
----------------
* Create a \*.php file, like *tracker/tracker_test.php* (or just copy/modify this file).
* Frequent modifications:
   * Check the variable *$midpoint*; it controls the map position if no coordinate has been submitted.
   * Create a KML (or KMZ = zipped KML) file with the track. (*gpsbabel* is useful for converting.)
   * If a KML/KMZ file is used, set *$use_kml* to *"true"*.
* Create a Javascript file under _tracker/_ with the same name, but the extension _*.js_.
* Create a subdirectory under _tracker/_ with the same name, but no extension.
* Create a symlink from *tracker_current.js* to the current tracker, e.g. to *tracker/tracker_test.js*. (The form writes its data always to the file referenced by this symlink.)
* Make sure the Javascript file (e.g. *tracker/tracker_test.js*) and the subdirectory (e.g. *tracker/tracker_test/*) are writable by the PHP script.


Use the Tracker
---------------
* View the tracker:
   * just visit e.g. http://website.com/tracker/tracker_test.php
   * The website does not reload automatically. You have to click on the reload button.
* Send coordinates manually:
   * Use the form, e.g. http://website.com/tracker_form.php
   * The browser should fill in the GPS coordinates automatically. (They can also be entered manually.)
   * => If cookies are configured correctly, it should also remember the last coordinates.
   * => Check the timestamp of the coordinates. If it is older, the coordinates are probably also outdated.
   * Enter some text, or select a photo.
   * Click on the submit button.
* Configure auto tracking using *OsmAnd* (Android app):
   * Install *OsmAnd*: https://play.google.com/store/apps/details?id=net.osmand
   * Activate the recording plugin.
   * Configure the URL for online track recording; e.g.: https://website.com/tracker_form.php?lat={0}&lon={1}&comment=time{2}hdop{3}altitude{4}speed{5}&key=12345&acc2=5000&auto=1&lang=de
   * => Make sure the *key=* parameter is the same as in the file *tracker_form.php*.
   * => The *auto* parameter causes the tracker to display only small dots, instead of markers.
   * => Feel free to change the *comment* parameter as you like; but then you probably have to update the associated parsing function in *tracker_form.php* as well.
* Use auto tracking using *OsmAnd*:
   * Activate track recording. I recommend a slow rate, like 90 seconds.
   * Activate online track recording. I usually use the slowest rate, 5 minutes.
* Control/debugging: View the Javascript file in the browser, and edit manually if necessary.


Copyright and License
---------------------
Author: Christoph Moder

License: [CC-by-sa 4.0](http://creativecommons.org/licenses/by/4.0/) – short: Do whatever you want, but don’t remove my name.
