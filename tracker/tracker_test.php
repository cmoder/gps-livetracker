><!DOCTYPE html>
<html lang="de">
<head>
	<title>GPS-Tracker-Test</title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Expires" content="86400" />
	<meta http-equiv="Content-Language" content="de" />
	<meta name="author" content="" />
	<meta name="description" content="" />
	<meta name="keywords" lang="de" content="Tracker, GPS" />
	<meta name="keywords" lang="en" content="Tracker, GPS" />
	<meta name="date" content="2018-07-15T00:00:00+00:00" />
	<meta name="robots" content="follow" />
	<meta name="revisit-after" content="30 days" />
	<meta name="viewport" content="width=device-width, user-scalable=yes" />
	<meta name="HandheldFriendly" content="true" />

	<meta http-equiv="Content-Style-Type" content="text/css" />
	<link rel="stylesheet" type="text/css" href="../stylesheet.css" />

<?php 
	$kml_file = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['SERVER_NAME'] . dirname( $_SERVER['SCRIPT_NAME'] ) . "/tracker_test.kmz";
	$use_kml = "false";
	$midpoint = "48, 12";

	@require( "../tracker_leaflet.inc" );
?>

</head>

<body onLoad="create_map();">

<div id="navline"><a href="http://website.com/">Home</a> &gt; <a href="../index.php">GPS-Tracker</a> &gt; GPS-Tracker-Test</div>
	<hr />
<h1>GPS-Tracker-Test</h1>

<p>Nur eine Testseite.</p>

<p><a href="#map">Karte Fullscreen</a>; Overlays: <?php echo $layerswitcher_string; ?></p>

<p id="map" name="map"></p>

</body>
</html>

