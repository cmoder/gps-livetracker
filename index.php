<!DOCTYPE html>
<html lang="de">
<head>
	<title>GPS-Tracker</title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Expires" content="86400" />
	<meta http-equiv="Content-Language" content="de" />
	<meta name="author" content="" />
	<meta name="description" content="GPS-Tracker" />
	<meta name="keywords" lang="de" content="GPS, Tracker" />
	<meta name="date" content="2018-07-15T00:00:00+00:00" />
	<meta name="robots" content="follow" />
	<meta name="revisit-after" content="30 days" />
	<meta name="viewport" content="width=device-width, user-scalable=yes" />
	<meta name="HandheldFriendly" content="true" /> 

	<meta http-equiv="Content-Style-Type" content="text/css" />
	<link rel="stylesheet" type="text/css" href="stylesheet.css" />
</head>

<body>
	<h1>GPS-Tracker</h1>

	<p>Die folgenden Tracker sind vorhanden:</p>

	<ul>
<?php
	$trackers = array();
	$comments = array();
	$subdir = "tracker";

	$handle = opendir( $subdir );		// Verzeichnis �ffnen

	while( $entry = readdir( $handle ) )	// n�chsten Eintrag lesen, bis keiner mehr kommt
	{	
		if( is_file( "$subdir/$entry" ) && is_readable( "$subdir/$entry" ) && ( substr( $entry, -4 ) == '.php' ) && ( substr( $entry, 0, 8 ) == 'tracker_' ) )
		{
			$mtime = filemtime( substr_replace( "$subdir/$entry", "js", -3 ) );
			
			$trackers[$mtime] = $entry;
			$comments[$mtime] = shell_exec( 'grep -m 1 "&gt;" < "' . "$subdir/$entry" . '" | sed -e "s/^.*&gt;[[:space:]]*//; s/ ([^)]\+\"#map\"[^)]\+)//;" 2> /dev/null' );
		}
	}

	closedir( $handle );

	krsort( $trackers );

	foreach( $trackers as $time => $tracker )
	{
			printf( "\t<li><p><a href=\"%s\">%s</a> (%s)</p></li>\r\n\r\n", rawurlencode( $subdir ) . "/" . rawurlencode( $tracker ), $comments[$time], date( "Y-m-d", $time ) );
	}
?>
	</ul>

</body>
</html>
